# dead_css

This gem reports css selectors that are potentially unused in your codebase, i.e dead css. It proceeds in three steps:

1. Scans css selectors defined in the codebase
2. Looks for these selectors in the HTML and JS files of the codebase
3. Reports unmatched selectors

# Limitations

WARNING! This gem is stupid, that is:

* Dead selectors won't be automatically deleted, only reported
* There might be false positives because the codebase scanning is kept simple

For example in the following code:

```
var half = 'button-';
$(button).addClass(half + 'active');
```

The `button-active` class WON'T be detected. Thus, the class may be reported as dead
even though it clearly is.

# Installation

```
$ gem install dead_css
```

# Usage

```
$ dead_css -c <path/to/codebase>
```

For example: `$ dead_css -c .`

# Dependencies

* https://github.com/premailer/css_parser
