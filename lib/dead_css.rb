require 'css_parser'


module UnusedCSS
  include CssParser

  CSS_EXTENSIONS = ['css', 'scss']
  ASSETS_EXTENSIONS = ['html', 'js', 'coffee', 'haml']
  SPLIT_REGEX = /(?:\s|>|~)+/
  SELECTOR_REGEX = /((\.|#)\w+)/

  class Selector
    def initialize(sel)
      @sel = sel
    end

    def prefix
      @sel[0]
    end

    def value
      @sel[1..-1]
    end

    def to_s
      @sel
    end
  end

  def self.files_by_extensions(root, extensions)
    extensions.map { |ext| Dir.glob("#{root}/**/*.#{ext}") }.flatten
  end

  def self.css_selectors(path)
    parser = CssParser::Parser.new
    parser.load_file!(path)
    selectors = []
    parser.each_selector { |selector, declarations, specificity| selectors << selector }
    selectors.map(&:split).flatten.uniq
  end

  def self.elementary_selectors(composed_selector)
    composed_selector.split(/(?:\s|>|~)+/).map do |s|
      SELECTOR_REGEX.match(s)
    end.flatten.compact.map(&:to_s)
  end

  def self.unmatched_selectors(selectors, files)
    unmatched_selectors = Array.new(selectors)
    files.each do |file|
      lines = File.readlines(file)
      unmatched_selectors.delete_if { |selector| lines.grep(Regexp.new(selector.value)).size > 0 }
    end
    unmatched_selectors
  end
end
