Gem::Specification.new do |s|
  s.name        = 'dead_css'
  s.version     = '1.0.0'
  s.date        = '2017-05-19'
  s.summary     = "Reports unused css"
  s.description = "List potentially unused css selectors in a project"
  s.authors     = ["Lucas Deschamps"]
  s.email       = 'lucasdchamps@gmail.com'
  s.files       = ['lib/dead_css.rb']
  s.executables << 'dead_css'
  s.homepage    = 'http://rubygems.org/gems/dead_css'
  s.license     = 'MIT'
  s.add_runtime_dependency 'css_parser', '~> 1.5'
end
